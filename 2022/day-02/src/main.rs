use std::io::BufRead;

fn main() {
    let file = std::fs::read("plays.txt").unwrap();
    let score = file
        .lines()
        .map(|line| line.unwrap())
        .map(|line| {
            let mut words = line.split_whitespace();
            let play = Play::from_letters(
                words.next().unwrap().chars().next().unwrap(),
                words.next().unwrap().chars().next().unwrap(),
            );
            play.get_score()
        })
        .sum::<i32>();
    println!("Score: {score}");

    let file = std::fs::read("plays.txt").unwrap();
    let score = file
        .lines()
        .map(|line| line.unwrap())
        .map(|line| {
            let mut words = line.split_whitespace();
            let play = CoolPlay::from_letters(
                words.next().unwrap().chars().next().unwrap(),
                words.next().unwrap().chars().next().unwrap(),
            );
            play.get_score()
        })
        .sum::<i32>();
    println!("Better score: {score}");
}

struct CoolPlay {
    other: Hand,
    mine: PlayResult,
}

impl CoolPlay {
    fn from_letters(other: char, mine: char) -> Self {
        Self {
            other: Hand::from_letter(other),
            mine: PlayResult::from_letter(mine),
        }
    }

    fn get_score(&self) -> i32 {
        let my_hand = self.mine.hand_for(self.other);
        Play {
            other: self.other,
            mine: my_hand,
        }
        .get_score()
    }
}

struct Play {
    other: Hand,
    mine: Hand,
}

impl Play {
    fn from_letters(other: char, mine: char) -> Self {
        Self {
            other: Hand::from_letter(other),
            mine: Hand::from_letter(mine),
        }
    }

    fn get_score(&self) -> i32 {
        self.play_score() + self.mine.hand_score()
    }

    fn play_score(&self) -> i32 {
        self.mine.score_against(self.other)
    }
}

#[derive(PartialEq, Eq, Clone, Copy)]
enum Hand {
    Rock,
    Paper,
    Scissors,
}

impl Hand {
    fn from_letter(letter: char) -> Self {
        match letter {
            'A' | 'X' => Self::Rock,
            'B' | 'Y' => Self::Paper,
            'C' | 'Z' => Self::Scissors,
            _ => panic!("bad input"),
        }
    }

    fn hand_score(&self) -> i32 {
        match self {
            Hand::Rock => 1,
            Hand::Paper => 2,
            Hand::Scissors => 3,
        }
    }

    fn score_against(self, other: Hand) -> i32 {
        if self == other {
            3
        } else if self.wins_over(other) {
            6
        } else {
            0
        }
    }

    fn wins_over(self, other: Hand) -> bool {
        matches!(
            (self, other),
            (Hand::Rock, Hand::Scissors)
                | (Hand::Paper, Hand::Rock)
                | (Hand::Scissors, Hand::Paper)
        )
    }
}

enum PlayResult {
    Draw,
    Win,
    Lose,
}

impl PlayResult {
    fn from_letter(letter: char) -> Self {
        match letter {
            'X' => Self::Lose,
            'Y' => Self::Draw,
            'Z' => Self::Win,
            _ => panic!("bad input"),
        }
    }

    fn hand_for(&self, other: Hand) -> Hand {
        match self {
            PlayResult::Draw => other,
            PlayResult::Win => match other {
                Hand::Rock => Hand::Paper,
                Hand::Paper => Hand::Scissors,
                Hand::Scissors => Hand::Rock,
            },
            PlayResult::Lose => match other {
                Hand::Scissors => Hand::Paper,
                Hand::Rock => Hand::Scissors,
                Hand::Paper => Hand::Rock,
            },
        }
    }
}
