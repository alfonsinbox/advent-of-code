use std::io::BufRead;

fn main() {
    let file = std::fs::read("elves.txt").unwrap();
    let mut lines = file.lines().map(|line| line.unwrap());
    let mut calories = vec![];
    loop {
        let values = lines
            .by_ref()
            .take_while(|line| !line.is_empty())
            .map(|line| line.parse::<i32>().unwrap())
            .collect::<Vec<_>>();
        if values.is_empty() {
            break;
        }
        calories.push(values.iter().sum::<i32>());
    }
    calories.sort();
    let top_one = calories.iter().rev().take(1).sum::<i32>();
    println!("Top one {top_one}");
    let top_three = calories.iter().rev().take(3).sum::<i32>();
    println!("Top three {top_three}");
}
